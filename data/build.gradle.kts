
plugins {
    id("com.android.library")
    kotlin("android")
    kotlin("kapt")
}

val appConfig: AppConfiguration by rootProject.extra
val commonLibs: Libraries by rootProject.extra
val dataLibs: Libraries by rootProject.extra
val dataKaptLibs: Libraries by rootProject.extra
val kotlinVersion: String by rootProject.extra

android {
    compileSdkVersion(appConfig.android.targetApi)

    defaultConfig {
        minSdkVersion(appConfig.android.minApi)
        targetSdkVersion(appConfig.android.targetApi)
        versionCode = 1
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles("proguard-rules.pro")
        }
    }

    sourceSets {
        getByName("main") {
            java.srcDirs("src/main/kotlin")
        }
    }
}

dependencies {
    implementation(kotlin("stdlib-jdk7", kotlinVersion))

    api(project(":domain"))

    commonLibs.applyAll { implementation(it) }
    dataLibs.applyAll { implementation(it) }
    dataKaptLibs.applyAll { kapt(it) }
}
