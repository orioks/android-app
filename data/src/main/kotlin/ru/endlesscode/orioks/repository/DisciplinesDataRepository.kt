package ru.endlesscode.orioks.repository

import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.Discipline
import ru.endlesscode.miet.orioks.model.Event
import ru.endlesscode.miet.orioks.repository.DisciplinesRepository
import ru.endlesscode.orioks.converter.DisciplineConverter
import ru.endlesscode.orioks.converter.EventConverter
import ru.endlesscode.orioks.network.api.OrioksApi
import javax.inject.Inject

class DisciplinesDataRepository @Inject constructor(
    private val api: OrioksApi
) : DisciplinesRepository {

    private var disciplines: List<Discipline>? = null
    private val eventsCache: MutableMap<Int, List<Event>> = mutableMapOf()

    override fun getDisciplines(): Single<List<Discipline>> {
        disciplines?.let { return Single.just(it) }

        return getDisciplinesFromApi()
    }

    override fun getDiscipline(disciplineId: Int): Single<Discipline> {
        return getDisciplines()
            .map { disciplines ->
                disciplines.first { it.id == disciplineId }
            }
    }

    override fun getDisciplineEvents(disciplineId: Int): Single<List<Event>> {
        if (disciplineId in eventsCache) return Single.just(eventsCache.getValue(disciplineId))

        return getEventsFromApi(disciplineId)
    }

    private fun getDisciplinesFromApi(): Single<List<Discipline>> {
        return api.getCurrentDisciplines()
            .map { it.map(DisciplineConverter::fromNetwork) }
            .doAfterSuccess(::storeDisciplinesToMemory)
    }

    private fun getEventsFromApi(disciplineId: Int): Single<List<Event>> {
        return api.getDisciplineEvents(disciplineId)
            .map { it.map(EventConverter::fromNetwork) }
            .doAfterSuccess { storeEventsToMemory(disciplineId, it) }
    }

    private fun storeDisciplinesToMemory(disciplines: List<Discipline>) {
        this.disciplines = disciplines
    }

    private fun storeEventsToMemory(disciplineId: Int, events: List<Event>) {
        this.eventsCache[disciplineId] = events
    }
}
