package ru.endlesscode.orioks.repository

import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.Discipline
import ru.endlesscode.miet.orioks.model.Event
import ru.endlesscode.miet.orioks.repository.DisciplinesRepository
import java.util.*
import javax.inject.Inject

class DisciplinesMockRepository @Inject constructor() : DisciplinesRepository {

    private val disciplines = listOf(
        Discipline(
            controlForm = "Дифференцированный зачёт",
            currentGrade = 0f,
            department = "Институт нано- и микросистемной техники",
            examDate = null,
            id = 81631,
            maxGrade = 0f,
            name = "История и методология науки и техники в области конструирования и технологии электронных средств",
            teachers = null
        ),
        Discipline(
            controlForm = "Экзамен",
            currentGrade = 23f,
            department = "Институт нано- и микросистемной техники",
            examDate = Date(),
            id = 81639,
            maxGrade = 42f,
            name = "Схемотехническое проектирование электронных средств",
            teachers = listOf("Власов Владимир Михайлович", "Кропоткин Махаил Парисович")
        ),
        Discipline(
            controlForm = "Дифференцированный зачёт",
            currentGrade = 0f,
            department = "Институт нано- и микросистемной техники",
            examDate = null,
            id = 81645,
            maxGrade = 0f,
            name = "САПР Pro/Engineer",
            teachers = null
        )
    )

    override fun getDisciplines(): Single<List<Discipline>> {
        return Single.just(disciplines)
    }

    override fun getDiscipline(disciplineId: Int): Single<Discipline> {
        return Single.just(disciplines.first { it.id == disciplineId })
    }

    override fun getDisciplineEvents(disciplineId: Int): Single<List<Event>> {
        return Single.just(
            listOf(
                Event(
                    type = "Лекция",
                    name = "Синтез комбинационнх схем",
                    alias = "Л.1",
                    maxGrade = 2.5f,
                    currentGrade = 2f,
                    week = 1
                ),
                Event(
                    type = "Контрольная",
                    name = "Синтез комбинационнх схем",
                    alias = "К.Р.1",
                    maxGrade = 5f,
                    currentGrade = -1f,
                    week = 1
                ),
                Event(
                    type = "Лекция",
                    name = "Оценка эффекта факторизации",
                    alias = "Л.2",
                    maxGrade = 0f,
                    currentGrade = 0f,
                    week = 3
                )
            )
        )
    }
}
