package ru.endlesscode.orioks.repository

import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.Student
import ru.endlesscode.miet.orioks.repository.StudentRepository
import javax.inject.Inject

class StudentMockRepository @Inject constructor() : StudentRepository {

    override fun getStudent(): Single<Student> {
        return Single.just(Student(
            course = 1,
            recordBook = 8140000,
            department = "[Институт нано- и микросистемной техники]",
            fullName = "[Иванов Иван Петорвич]",
            group = "[П-12]",
            semester = 1,
            studyDirection = "[Конструирование и технология электронных средств]",
            studyProfile = "[Проектирование технических систем средствами 3D-моделирования]",
            year = "[2018-2019]"
        ))
    }
}
