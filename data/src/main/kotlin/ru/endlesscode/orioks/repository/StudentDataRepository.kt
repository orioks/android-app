package ru.endlesscode.orioks.repository

import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.Student
import ru.endlesscode.miet.orioks.repository.StudentRepository
import ru.endlesscode.orioks.converter.StudentConverter
import ru.endlesscode.orioks.network.api.OrioksApi
import javax.inject.Inject

class StudentDataRepository @Inject constructor(
    private val api: OrioksApi
) : StudentRepository {

    private var student: Student? = null

    override fun getStudent(): Single<Student> {
        student?.let { return Single.just(it) }

        return getStudentFromApi()
    }

    private fun getStudentFromApi(): Single<Student> {
        return api.getStudent()
            .map(StudentConverter::fromNetwork)
            .doAfterSuccess(::storeToMemory)
    }

    private fun storeToMemory(student: Student) {
        this.student = student
    }
}
