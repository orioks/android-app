package ru.endlesscode.orioks.converter

import ru.endlesscode.miet.orioks.model.Discipline
import ru.endlesscode.orioks.entity.DisciplineResponse
import ru.endlesscode.orioks.util.makeSureNotNull
import ru.endlesscode.orioks.util.toDate

object DisciplineConverter {

    fun fromNetwork(source: DisciplineResponse): Discipline {
        with(source) {
            return Discipline(
                controlForm = controlForm,
                currentGrade = currentGrade,
                maxGrade = maxGrade.makeSureNotNull("max_grade"),
                teachers = teachers,
                department = department.makeSureNotNull("department"),
                name = name.makeSureNotNull("name"),
                examDate = examDate?.toDate(),
                id = id.makeSureNotNull("id")
            )
        }
    }
}
