package ru.endlesscode.orioks.converter

import ru.endlesscode.miet.orioks.model.Student
import ru.endlesscode.orioks.entity.StudentResponse
import ru.endlesscode.orioks.util.makeSureNotNull

object StudentConverter {

    fun fromNetwork(source: StudentResponse): Student {
        with(source) {
            return Student(
                course = course.makeSureNotNull("course"),
                recordBook = recordBookId.makeSureNotNull("credit_book_id"),
                department = department.makeSureNotNull("department"),
                fullName = fullName.makeSureNotNull("full_name"),
                group = group.makeSureNotNull("group"),
                semester = semester.makeSureNotNull("semester"),
                studyDirection = studyDirection.makeSureNotNull("study_direction"),
                studyProfile = studyProfile.makeSureNotNull("study_profile"),
                year = year.makeSureNotNull("year")
            )
        }
    }
}
