package ru.endlesscode.orioks.converter

import ru.endlesscode.miet.orioks.model.Event
import ru.endlesscode.orioks.entity.EventResponse
import ru.endlesscode.orioks.util.makeSureNotNull

object EventConverter {

    fun fromNetwork(source: EventResponse): Event {
        with(source) {

            return Event(
                alias = alias,
                type = type.makeSureNotNull("type"),
                name = name,
                week = week.makeSureNotNull("week"),
                currentGrade = currentGrade,
                maxGrade = maxGrade.makeSureNotNull("max_grade")
            )
        }
    }
}
