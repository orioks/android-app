package ru.endlesscode.orioks.converter

import ru.endlesscode.miet.orioks.model.AuthData
import ru.endlesscode.orioks.entity.AuthResponse
import ru.endlesscode.orioks.util.makeSureNotNull


object AuthDataConverter {

    fun fromNetwork(source: AuthResponse): AuthData {
        return AuthData(
            token = source.token.makeSureNotNull("token")
        )
    }
}
