package ru.endlesscode.orioks.provider

import io.reactivex.Completable
import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.AuthData
import ru.endlesscode.miet.orioks.provider.BasicAuthProvider
import ru.endlesscode.orioks.converter.AuthDataConverter
import ru.endlesscode.orioks.network.api.OrioksApi
import javax.inject.Inject


class OrioksAuthProvider @Inject constructor(
    private val api: OrioksApi
) : BasicAuthProvider {

    override fun signIn(): Single<AuthData> {
        return api.signIn().map(AuthDataConverter::fromNetwork)
    }

    override fun signOut(token: String): Completable {
        //TODO: make real request
        return Completable.complete()
    }
}
