package ru.endlesscode.orioks.provider

import android.content.Context
import android.content.SharedPreferences
import ru.endlesscode.miet.orioks.provider.ProfileHolder
import ru.endlesscode.miet.orioks.util.clear
import ru.endlesscode.miet.orioks.util.isClear
import ru.endlesscode.orioks.util.edit
import ru.endlesscode.orioks.util.putString
import javax.inject.Inject


class PrefsProfileHolder @Inject constructor(
    private val context: Context
) : ProfileHolder {

    companion object {
        private const val AUTH_DATA = "profile_data"
        private const val TOKEN = "token"
        private const val USERNAME = "username"
        private const val PASSWORD = "password"
    }

    override var token: String
        get() = prefs.getString(TOKEN, "").orEmpty()
        set(value) {
            prefs.putString(TOKEN, value)
        }

    override var username: String
        get() = prefs.getString(USERNAME, "").orEmpty()
        set(value) {
            prefs.putString(USERNAME, value)
        }

    override var password: CharArray = charArrayOf()

    override var isLogsOut: Boolean = false

    override val isAuthorized: Boolean
        get() = !isLogsOut && token.isNotEmpty() && password.isClear()

    private val prefs: SharedPreferences by lazy {
        context.getSharedPreferences(AUTH_DATA, Context.MODE_PRIVATE)
    }

    override fun clearSecretData() {
        prefs.edit {
            remove(TOKEN)
            remove(PASSWORD)
        }
    }
}
