package ru.endlesscode.orioks.entity


data class AuthResponse(
    val token: String? = null
)
