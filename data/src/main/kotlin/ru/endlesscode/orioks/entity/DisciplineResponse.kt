package ru.endlesscode.orioks.entity

data class DisciplineResponse(
    val controlForm: String? = null,
    val currentGrade: Float? = null,
    val department: String? = null,
    val examDate: String? = null,
    val id: Int? = null,
    val maxGrade: Float? = null,
    val name: String? = null,
    val teachers: List<String>? = null
)
