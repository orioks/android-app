package ru.endlesscode.orioks.entity

data class StudentResponse(
    val course: Int? = null,
    val recordBookId: Int? = null,
    val department: String? = null,
    val fullName: String? = null,
    val group: String? = null,
    val semester: Int? = null,
    val studyDirection: String? = null,
    val studyProfile: String? = null,
    val year: String? = null
)
