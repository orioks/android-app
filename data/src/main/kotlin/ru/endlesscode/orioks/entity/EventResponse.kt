package ru.endlesscode.orioks.entity

data class EventResponse(
    val alias: String? = null,
    val type: String? = null,
    val name: String? = null,
    val week: Int? = null,
    val currentGrade: Float? = null,
    val maxGrade: Float? = null
)
