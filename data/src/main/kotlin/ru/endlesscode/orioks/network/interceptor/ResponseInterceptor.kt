package ru.endlesscode.orioks.network.interceptor

import com.google.gson.JsonObject
import com.google.gson.JsonParser
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody
import javax.inject.Inject


class ResponseInterceptor @Inject constructor() : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
        val response = chain.proceed(request)
        val body = response.peekBody(Long.MAX_VALUE)

        if (!response.isSuccessful && body.isJson) {
            val json = body.string()
            val jsonBody = JsonParser().parse(json).asJsonObject

            throwErrorWithRightMessage(jsonBody)
        }

        return response
    }

    private fun throwErrorWithRightMessage(jsonBody: JsonObject) {
        if (jsonBody.has("error")) {
            val errorMessage = jsonBody.get("error").asString
            throw RuntimeException(errorMessage)
        }
    }

    private inline val ResponseBody?.isJson: Boolean
        get() = this?.contentType()?.subtype() == "json"

}
