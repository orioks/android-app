package ru.endlesscode.orioks.network.api

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import ru.endlesscode.orioks.entity.AuthResponse
import ru.endlesscode.orioks.entity.DisciplineResponse
import ru.endlesscode.orioks.entity.EventResponse
import ru.endlesscode.orioks.entity.StudentResponse


interface OrioksApi {

    @GET("auth")
    fun signIn(): Single<AuthResponse>

    @GET("student")
    fun getStudent(): Single<StudentResponse>

    @GET("student/disciplines")
    fun getCurrentDisciplines(): Single<List<DisciplineResponse>>

    @GET("student/disciplines/{disciplineId}/events")
    fun getDisciplineEvents(@Path("disciplineId") disciplineId: Int): Single<List<EventResponse>>
}
