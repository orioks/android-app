package ru.endlesscode.orioks.network.interceptor

import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.Response
import ru.endlesscode.miet.orioks.provider.ProfileHolder
import javax.inject.Inject


class BasicAuthenticator @Inject constructor(
    private val holder: ProfileHolder
) : Interceptor {

    companion object {
        private const val HEADER_AUTH = "Authorization"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        chain.request().header(HEADER_AUTH)?.let { authHeader ->
            if (authHeader.startsWith("Bearer")) {
                holder.clearSecretData()
            }

            return chain.proceed(chain.request())
        }

        val credentials = when {
            holder.isAuthorized -> "Bearer ${holder.token}"
            else -> getBasicAuthHeader()
        }

        return chain.proceed(chain.request().newBuilder()
            .addHeader(HEADER_AUTH, credentials)
            .addHeader("User-Agent", "orioks_app/0.1 Android")
            .addHeader("Accept", "application/json")
            .build())
    }

    private fun getBasicAuthHeader(): String {
        holder.isLogsOut = false
        return Credentials.basic(holder.username, String(holder.password))
    }
}
