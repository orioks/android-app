package ru.endlesscode.orioks.util

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


private val serverDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.US)

internal fun String.toDate(): Date? {
    return try {
        serverDateFormat.parse(this)
    } catch (e: ParseException) {
        null
    }
}
