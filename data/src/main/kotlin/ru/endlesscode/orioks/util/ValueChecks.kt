package ru.endlesscode.orioks.util


fun <T> T?.makeSureNotNull(name: String? = null): T {
    return this ?: error("Field ${name?.let { "'$name' " }}shouldn't be null")
}
