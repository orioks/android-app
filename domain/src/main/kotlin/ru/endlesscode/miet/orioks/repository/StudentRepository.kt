package ru.endlesscode.miet.orioks.repository

import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.Student

interface StudentRepository {
    fun getStudent(): Single<Student>
}
