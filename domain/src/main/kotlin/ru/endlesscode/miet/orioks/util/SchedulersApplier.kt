package ru.endlesscode.miet.orioks.util

import io.reactivex.CompletableTransformer
import io.reactivex.FlowableTransformer
import io.reactivex.Scheduler
import io.reactivex.SingleTransformer
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject


class SchedulersApplier @Inject constructor(private val mainThreadScheduler: Scheduler) {

    fun ioToMainCompletable(): CompletableTransformer {
        return CompletableTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(mainThreadScheduler)
        }
    }

    fun <T> ioToMainSingle(): SingleTransformer<T, T> {
        return SingleTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(mainThreadScheduler)
        }
    }

    fun <T> ioToMainFlowable(): FlowableTransformer<T, T> {
        return FlowableTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(mainThreadScheduler)
        }
    }

    fun ioCompletable(): CompletableTransformer {
        return CompletableTransformer {
            it.subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
        }
    }
}
