package ru.endlesscode.miet.orioks.provider


interface ProfileHolder {
    var username: String
    var password: CharArray
    var token: String
    var isLogsOut: Boolean

    val isAuthorized: Boolean

    fun clearSecretData()
}
