package ru.endlesscode.miet.orioks.provider

import io.reactivex.Completable
import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.AuthData


interface BasicAuthProvider {

    fun signIn(): Single<AuthData>
    fun signOut(token: String): Completable
}
