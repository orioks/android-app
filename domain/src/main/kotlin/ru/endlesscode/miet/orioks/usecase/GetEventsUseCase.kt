package ru.endlesscode.miet.orioks.usecase

import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.Event
import ru.endlesscode.miet.orioks.repository.DisciplinesRepository
import ru.endlesscode.miet.orioks.util.SchedulersApplier
import javax.inject.Inject


class GetEventsUseCase @Inject constructor(
    private val repository: DisciplinesRepository,
    private val applier: SchedulersApplier
) {

    fun getDisciplineEvents(disciplineId: Int): Single<List<Event>> {
        return repository.getDisciplineEvents(disciplineId)
            .compose(applier.ioToMainSingle())
    }
}
