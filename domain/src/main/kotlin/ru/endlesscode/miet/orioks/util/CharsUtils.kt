package ru.endlesscode.miet.orioks.util


fun CharArray.clear() {
    this.fill('\u0000')
}

fun CharArray.isClear(): Boolean {
    return this.all { it == '\u0000' }
}
