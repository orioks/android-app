package ru.endlesscode.miet.orioks.model

data class Event(
    val alias: String?,
    val type: String,
    val name: String?,
    val week: Int,
    val currentGrade: Float?,
    val maxGrade: Float
)
