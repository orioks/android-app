package ru.endlesscode.miet.orioks.usecase

import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.Discipline
import ru.endlesscode.miet.orioks.repository.DisciplinesRepository
import ru.endlesscode.miet.orioks.util.SchedulersApplier
import javax.inject.Inject


class GetDisciplinesUseCase @Inject constructor(
    private val repository: DisciplinesRepository,
    private val applier: SchedulersApplier
) {

    fun getDisciplines(): Single<List<Discipline>> {
        return repository.getDisciplines()
            .compose(applier.ioToMainSingle())
    }

    fun getDiscipline(disciplineId: Int): Single<Discipline> {
        return repository.getDiscipline(disciplineId)
            .compose(applier.ioToMainSingle())
    }
}
