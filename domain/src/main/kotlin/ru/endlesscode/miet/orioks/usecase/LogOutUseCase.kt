package ru.endlesscode.miet.orioks.usecase

import io.reactivex.Completable
import ru.endlesscode.miet.orioks.provider.BasicAuthProvider
import ru.endlesscode.miet.orioks.provider.ProfileHolder
import ru.endlesscode.miet.orioks.util.SchedulersApplier
import javax.inject.Inject

class LogOutUseCase @Inject constructor(
    private val authProvider: BasicAuthProvider,
    private val profileHolder: ProfileHolder,
    private val applier: SchedulersApplier
) {

    fun logOut(): Completable {
        profileHolder.isLogsOut = true
        return authProvider.signOut(profileHolder.token)
            .compose(applier.ioToMainCompletable())
            .doOnComplete(::clearAuthData)
    }

    fun clearAuthData() {
        profileHolder.clearSecretData()
    }
}
