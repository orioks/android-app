package ru.endlesscode.miet.orioks.usecase

import io.reactivex.Completable
import ru.endlesscode.miet.orioks.model.AuthData
import ru.endlesscode.miet.orioks.provider.BasicAuthProvider
import ru.endlesscode.miet.orioks.provider.ProfileHolder
import ru.endlesscode.miet.orioks.util.SchedulersApplier
import ru.endlesscode.miet.orioks.util.clear
import javax.inject.Inject


class AuthUseCase @Inject constructor(
    private val authProvider: BasicAuthProvider,
    private val applier: SchedulersApplier,
    private val profileHolder: ProfileHolder
) {

    val isAuthorized get() = profileHolder.isAuthorized
    val cachedLogin get() = profileHolder.username

    fun logIn(login: String, password: CharArray): Completable {
        profileHolder.username = login
        profileHolder.password = password

        return authProvider.signIn()
            .flatMapCompletable(::processAuthData)
            .compose(applier.ioToMainCompletable())
    }

    private fun processAuthData(data: AuthData): Completable {
        with(profileHolder) {
            token = data.token
            password.clear()
        }

        return Completable.complete()
    }
}
