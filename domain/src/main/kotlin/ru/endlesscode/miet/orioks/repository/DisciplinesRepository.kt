package ru.endlesscode.miet.orioks.repository

import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.Discipline
import ru.endlesscode.miet.orioks.model.Event

interface DisciplinesRepository {

    fun getDisciplines(): Single<List<Discipline>>
    fun getDiscipline(disciplineId: Int): Single<Discipline>
    fun getDisciplineEvents(disciplineId: Int): Single<List<Event>>
}
