package ru.endlesscode.miet.orioks.model


data class AuthData(
    val token: String
)
