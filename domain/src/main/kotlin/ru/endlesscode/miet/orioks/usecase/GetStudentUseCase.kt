package ru.endlesscode.miet.orioks.usecase

import io.reactivex.Single
import ru.endlesscode.miet.orioks.model.Student
import ru.endlesscode.miet.orioks.repository.StudentRepository
import ru.endlesscode.miet.orioks.util.SchedulersApplier
import javax.inject.Inject

class GetStudentUseCase @Inject constructor(
    private val repository: StudentRepository,
    private val applier: SchedulersApplier
) {

    fun getStudent(): Single<Student> {
        return repository.getStudent()
            .compose(applier.ioToMainSingle())
    }
}
