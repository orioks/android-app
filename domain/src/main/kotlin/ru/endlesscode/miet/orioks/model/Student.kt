package ru.endlesscode.miet.orioks.model

data class Student(
    val course: Int,
    val recordBook: Int,
    val department: String,
    val fullName: String,
    val group: String,
    val semester: Int,
    val studyDirection: String,
    val studyProfile: String,
    val year: String
)
