package ru.endlesscode.miet.orioks.model

import java.util.*

data class Discipline(
    val controlForm: String?,
    val currentGrade: Float?,
    val department: String,
    val examDate: Date?,
    val id: Int,
    val maxGrade: Float,
    val name: String,
    val teachers: List<String>?
)
