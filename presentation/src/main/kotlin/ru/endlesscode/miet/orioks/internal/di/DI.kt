package ru.endlesscode.miet.orioks.internal.di

import android.app.Application
import ru.endlesscode.miet.orioks.internal.di.holder.ApplicationComponentHolder
import ru.endlesscode.miet.orioks.internal.di.holder.AuthComponentHolder
import ru.endlesscode.miet.orioks.internal.di.holder.DisciplinesComponentHolder
import ru.endlesscode.miet.orioks.internal.di.holder.MainComponentHolder
import ru.endlesscode.miet.orioks.internal.di.holder.SettingsComponentHolder


object DI {

    val app: ApplicationComponentHolder by lazy { ApplicationComponentHolder(appInstance) }

    val auth: AuthComponentHolder get() = app.auth

    val main: MainComponentHolder get() = app.main
    val disciplines: DisciplinesComponentHolder get() = main.disciplines
    val settings: SettingsComponentHolder get() = main.settings

    private lateinit var appInstance: Application

    fun init(appInstance: Application) {
        DI.appInstance = appInstance
    }
}
