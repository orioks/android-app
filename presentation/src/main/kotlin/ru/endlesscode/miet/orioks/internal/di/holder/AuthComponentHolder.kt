package ru.endlesscode.miet.orioks.internal.di.holder

import ru.endlesscode.miet.orioks.internal.di.component.ApplicationComponent
import ru.endlesscode.miet.orioks.internal.di.component.AuthComponent
import ru.endlesscode.miet.orioks.internal.di.holder.common.SubComponentHolder


class AuthComponentHolder(
    parent: ApplicationComponentHolder
) : SubComponentHolder<AuthComponent, ApplicationComponent>(parent) {

    override fun provideInternal(parentComponent: ApplicationComponent): AuthComponent {
        return parentComponent.authComponent().build()
    }
}
