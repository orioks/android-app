package ru.endlesscode.miet.orioks.presentation.main.presenter

import com.arellomobile.mvp.InjectViewState
import ru.endlesscode.miet.orioks.R
import ru.endlesscode.miet.orioks.converter.StudentViewConverter
import ru.endlesscode.miet.orioks.internal.Screens
import ru.endlesscode.miet.orioks.internal.di.Local
import ru.endlesscode.miet.orioks.internal.di.MainScope
import ru.endlesscode.miet.orioks.model.Student
import ru.endlesscode.miet.orioks.presentation.common.presenter.BasePresenter
import ru.endlesscode.miet.orioks.presentation.main.view.MainMenuView
import ru.endlesscode.miet.orioks.usecase.GetStudentUseCase
import ru.endlesscode.miet.orioks.usecase.LogOutUseCase
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@MainScope
@InjectViewState
class MainMenuPresenter @Inject constructor(
    private val globalRouter: Router,
    @Local private val cicerone: Cicerone<Router>,
    private val getStudentUseCase: GetStudentUseCase,
    private val logOutUseCase: LogOutUseCase
) : BasePresenter<MainMenuView>() {

    private val router get() = cicerone.router
    private val navigatorHolder get() = cicerone.navigatorHolder
    private var currentScreen: String = Screens.PROGRESS

    override fun onFirstViewAttach() {
        getStudent()
    }

    fun onNavItemSelected(itemId: Int) {
        val screen = when (itemId) {
            //R.id.nav_news -> Screens.NEWS
            R.id.nav_learning -> Screens.PROGRESS
            //R.id.nav_schedule -> Screens.SCHEDULE
            //R.id.nav_faq -> Screens.FAQ
            //R.id.nav_settings -> Screens.SETTINGS
            else -> error("Unknown navigation menu item!")
        }

        // Use global router instead
        if (screen == Screens.SETTINGS) {
            globalRouter.navigateTo(screen)
            return
        }

        currentScreen = screen
        navigateToCurrentScreen()
    }

    fun afterResume(navigator: Navigator) {
        navigatorHolder.setNavigator(navigator)
    }

    fun beforePause() {
        navigatorHolder.removeNavigator()
    }

    private fun getStudent() {
        safeSubscribe {
            getStudentUseCase.getStudent()
                .subscribe(::onSuccess, ::onError)
        }
    }

    private fun onSuccess(student: Student) {
        viewState.showStudentData(StudentViewConverter.fromDomain(student))
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
        globalRouter.replaceScreen(Screens.AUTH)
        logOutUseCase.clearAuthData()
    }

    private fun navigateToCurrentScreen() {
        router.navigateTo(currentScreen)
    }
}
