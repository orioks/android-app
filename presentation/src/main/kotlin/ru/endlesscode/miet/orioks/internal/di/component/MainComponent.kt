package ru.endlesscode.miet.orioks.internal.di.component

import dagger.Subcomponent
import ru.endlesscode.miet.orioks.internal.di.MainScope
import ru.endlesscode.miet.orioks.internal.di.module.MainMenuNavigationModule
import ru.endlesscode.miet.orioks.internal.di.module.StudentModule
import ru.endlesscode.miet.orioks.presentation.main.fragment.MainMenuFragment


@MainScope
@Subcomponent(modules = [MainMenuNavigationModule::class, StudentModule::class])
interface MainComponent {

    fun inject(fragment: MainMenuFragment)

    fun disciplinesComponent(): DisciplinesComponent.Builder
    fun settingsComponent(): SettingsComponent.Builder

    @Subcomponent.Builder
    interface Builder {
        fun build(): MainComponent
    }
}
