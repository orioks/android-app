package ru.endlesscode.miet.orioks.presentation.disciplines.fragment

import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.view.View
import android.widget.LinearLayout
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.item_event.view.*
import kotlinx.android.synthetic.main.item_event_week.view.*
import kotlinx.android.synthetic.main.layout_discipline_data.*
import kotlinx.android.synthetic.main.screen_discipline.*
import ru.endlesscode.miet.orioks.R
import ru.endlesscode.miet.orioks.internal.di.DI
import ru.endlesscode.miet.orioks.model.DisciplineViewData
import ru.endlesscode.miet.orioks.model.EventViewData
import ru.endlesscode.miet.orioks.presentation.common.fragment.BaseFragment
import ru.endlesscode.miet.orioks.presentation.disciplines.presenter.DisciplinePresenter
import ru.endlesscode.miet.orioks.presentation.disciplines.view.DisciplineView
import ru.endlesscode.miet.orioks.util.chunkedBy
import ru.endlesscode.miet.orioks.util.getColorCompat
import ru.endlesscode.miet.orioks.util.inflateChild
import ru.endlesscode.miet.orioks.util.show
import javax.inject.Inject

class DisciplineFragment : BaseFragment(), DisciplineView {

    companion object {
        private const val ARG_ID = "id"

        fun newInstance(disciplineId: Int): DisciplineFragment {
            return DisciplineFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_ID, disciplineId)
                }
            }
        }
    }

    override val layoutId = R.layout.screen_discipline

    @Inject
    @InjectPresenter
    internal lateinit var presenter: DisciplinePresenter

    @ProvidePresenter
    fun providePresenter(): DisciplinePresenter {
        return presenter.apply {
            val id = arguments?.getInt(ARG_ID) ?: error("Discipline id should be passed")
            init(id)
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        DI.disciplines.provideComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerListeners()
    }

    override fun onDestroy() {
        super.onDestroy()

        DI.disciplines.onDependencyReleased()
    }

    override fun showDiscipline(discipline: DisciplineViewData) {
        with(discipline) {
            discipline_title_text_view.text = name
            discipline_title_text_view.movementMethod = ScrollingMovementMethod()

            type_text_view.text = controlForm
            teachers?.let {
                teachers_text_view.text = it.joinToString("\n")
                teachers_text_view.show()
                teachers_image_view.show()
            }
            department_text_view.text = department
        }
    }

    override fun initEvents(events: List<EventViewData>) {
        if (events.isEmpty()) {
            showEmptyState()
            return
        }

        events.chunkedBy { prev, current -> prev.week == current.week }
            .forEach { events_weeks_linear_layout.addEventWeek(it) }
    }

    private fun registerListeners() {
        toolbar.setNavigationOnClickListener { presenter.onNavigationClicked() }
    }

    private fun showEmptyState() {
        no_events_text_view.show()
    }

    private fun LinearLayout.addEventWeek(events: List<EventViewData>) {
        val eventWeekView = this.inflateChild<View>(R.layout.item_event_week)
        eventWeekView.week_text_view.text = events.first().week.toString()
        events.forEach {
            eventWeekView.events_list_linear_layout.addEvent(it)
        }

        this.addView(eventWeekView)
    }

    private fun LinearLayout.addEvent(event: EventViewData) {
        val eventView = this.inflateChild<View>(R.layout.item_event)
        with(eventView) {
            type_text_view.text = event.type
            event.name?.let { name ->
                val fullName = name + (event.alias?.let { " ($it)" } ?: "")
                name_text_view.text = fullName
                name_text_view.show()
            }

            rank_current_text_view.text = event.currentGrade
            rank_max_text_view.text = getString(R.string.event_max_rank, event.maxGrade)

            if (event.colorId != 0) {
                val color = context.getColorCompat(event.colorId)
                rank_current_text_view.setTextColor(color)
            }
        }

        this.addView(eventView)
    }
}
