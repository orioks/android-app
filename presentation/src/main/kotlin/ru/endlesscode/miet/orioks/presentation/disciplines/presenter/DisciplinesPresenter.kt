package ru.endlesscode.miet.orioks.presentation.disciplines.presenter

import com.arellomobile.mvp.InjectViewState
import ru.endlesscode.miet.orioks.converter.DisciplineViewConverter
import ru.endlesscode.miet.orioks.internal.Screens
import ru.endlesscode.miet.orioks.internal.di.DisciplinesScope
import ru.endlesscode.miet.orioks.model.DisciplineViewData
import ru.endlesscode.miet.orioks.presentation.common.presenter.BasePresenter
import ru.endlesscode.miet.orioks.presentation.disciplines.view.DisciplinesView
import ru.endlesscode.miet.orioks.usecase.GetDisciplinesUseCase
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@DisciplinesScope
@InjectViewState
class DisciplinesPresenter @Inject constructor(
    private val getDisciplinesUseCase: GetDisciplinesUseCase,
    private val router: Router
) : BasePresenter<DisciplinesView>() {

    override fun onFirstViewAttach() {
        loadDisciplines()
    }

    fun onDisciplineClicked(discipline: DisciplineViewData) {
        router.navigateTo(Screens.DISCIPLINE, discipline.id)
    }

    private fun loadDisciplines() {
        safeSubscribe {
            getDisciplinesUseCase.getDisciplines()
                .map { it.map(DisciplineViewConverter::fromDomain) }
                .subscribe(::onSuccess, ::onError)
        }
    }

    private fun onSuccess(disciplines: List<DisciplineViewData>) {
        viewState.initDisciplines(disciplines)
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
        // TODO: print error
    }
}
