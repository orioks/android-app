package ru.endlesscode.miet.orioks.internal.di

import javax.inject.Scope


@Scope
@Retention
annotation class ApplicationScope


@Scope
@Retention
annotation class AuthScope


@Scope
@Retention
annotation class MainScope


@Scope
@Retention
annotation class DisciplinesScope


@Scope
@Retention
annotation class SettingsScope
