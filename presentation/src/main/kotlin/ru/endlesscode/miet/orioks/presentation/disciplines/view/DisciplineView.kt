package ru.endlesscode.miet.orioks.presentation.disciplines.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.endlesscode.miet.orioks.model.DisciplineViewData
import ru.endlesscode.miet.orioks.model.EventViewData


@StateStrategyType(AddToEndSingleStrategy::class)
interface DisciplineView : MvpView {

    fun showDiscipline(discipline: DisciplineViewData)
    fun initEvents(events: List<EventViewData>)
}
