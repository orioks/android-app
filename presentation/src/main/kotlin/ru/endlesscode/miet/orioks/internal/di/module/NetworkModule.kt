package ru.endlesscode.miet.orioks.internal.di.module

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import ru.endlesscode.miet.orioks.internal.di.ApplicationScope
import ru.endlesscode.orioks.network.api.OrioksApi


@Module(includes = [RetrofitModule::class, SessionModule::class])
object NetworkModule {

    @JvmStatic
    @Provides
    @ApplicationScope
    fun provideApi(retrofit: Retrofit): OrioksApi = retrofit.create(OrioksApi::class.java)
}
