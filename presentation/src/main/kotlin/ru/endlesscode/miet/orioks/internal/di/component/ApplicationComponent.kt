package ru.endlesscode.miet.orioks.internal.di.component

import dagger.Component
import ru.endlesscode.miet.orioks.internal.di.ApplicationScope
import ru.endlesscode.miet.orioks.internal.di.module.AuthModule
import ru.endlesscode.miet.orioks.internal.di.module.ContextModule
import ru.endlesscode.miet.orioks.internal.di.module.NavigationModule
import ru.endlesscode.miet.orioks.internal.di.module.NetworkModule
import ru.endlesscode.miet.orioks.presentation.main.activity.MainActivity


@ApplicationScope
@Component(modules = [NavigationModule::class, ContextModule::class, NetworkModule::class, AuthModule::class])
interface ApplicationComponent {

    fun inject(activity: MainActivity)

    fun mainComponent(): MainComponent.Builder
    fun authComponent(): AuthComponent.Builder
}
