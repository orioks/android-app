package ru.endlesscode.miet.orioks.model

data class StudentViewData(
    val fullName: String,
    val group: String,
    val recordBook: String
)
