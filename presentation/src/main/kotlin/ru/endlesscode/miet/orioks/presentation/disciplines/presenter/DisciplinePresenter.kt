package ru.endlesscode.miet.orioks.presentation.disciplines.presenter

import com.arellomobile.mvp.InjectViewState
import ru.endlesscode.miet.orioks.converter.DisciplineViewConverter
import ru.endlesscode.miet.orioks.converter.EventViewConverter
import ru.endlesscode.miet.orioks.model.DisciplineViewData
import ru.endlesscode.miet.orioks.model.EventViewData
import ru.endlesscode.miet.orioks.presentation.common.presenter.BasePresenter
import ru.endlesscode.miet.orioks.presentation.disciplines.view.DisciplineView
import ru.endlesscode.miet.orioks.usecase.GetDisciplinesUseCase
import ru.endlesscode.miet.orioks.usecase.GetEventsUseCase
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@InjectViewState
class DisciplinePresenter @Inject constructor(
    private val disciplinesUseCase: GetDisciplinesUseCase,
    private val eventsUseCase: GetEventsUseCase,
    private val router: Router
) : BasePresenter<DisciplineView>() {

    private var disciplineId: Int = 0

    fun init(disciplineId: Int) {
        this.disciplineId = disciplineId
    }

    override fun onFirstViewAttach() {
        loadDiscipline()
        loadEvents()
    }

    fun onNavigationClicked() {
        router.exit()
    }

    private fun loadDiscipline() {
        safeSubscribe {
            disciplinesUseCase.getDiscipline(disciplineId)
                .map(DisciplineViewConverter::fromDomain)
                .subscribe(::onDisciplineLoaded, ::onError)
        }
    }

    private fun loadEvents() {
        safeSubscribe {
            eventsUseCase.getDisciplineEvents(disciplineId)
                .map { it.map(EventViewConverter::fromDomain) }
                .subscribe(::onEventsLoaded, ::onError)
        }
    }

    private fun onDisciplineLoaded(discipline: DisciplineViewData) {
        viewState.showDiscipline(discipline)
    }

    private fun onEventsLoaded(events: List<EventViewData>) {
        viewState.initEvents(events)
    }

    private fun onError(throwable: Throwable) {
        throwable.printStackTrace()
        // TODO: print error
    }
}
