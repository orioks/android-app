package ru.endlesscode.miet.orioks.util

import android.content.Context
import android.support.annotation.StringRes
import android.support.v7.app.AlertDialog


fun alert(
    context: Context,
    @StringRes titleId: Int,
    @StringRes messageId: Int,
    block: AlertDialog.Builder.() -> Unit = {}
) {
    AlertDialog.Builder(context)
        .setTitle(titleId)
        .setMessage(messageId)
        .apply(block)
        .create()
        .show()
}

fun AlertDialog.Builder.positiveButton(
    @StringRes stringId: Int = android.R.string.ok,
    action: () -> Unit = {}
) {
    setPositiveButton(stringId) { _, _ -> action() }
}

fun AlertDialog.Builder.negativeButton(
    @StringRes stringId: Int = android.R.string.cancel,
    action: () -> Unit = {}
) {
    setNegativeButton(stringId) { _, _ -> action() }
}
