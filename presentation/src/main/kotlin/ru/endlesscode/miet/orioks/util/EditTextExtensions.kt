package ru.endlesscode.miet.orioks.util

import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject


fun EditText.asObservable(): Observable<CharSequence> {
    return PublishSubject.create {
        this.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                it.onNext(s)
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
                // Ignored
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                // Ignored
            }
        })
    }
}

fun EditText.gainFocus() {
    this.requestFocus()

    val inputMethodManager = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
}

fun Editable.getChars(): CharArray {
    val chars = CharArray(length)
    getChars(0, length, chars, 0)

    return chars
}

fun EditText.setText(chars: CharArray) {
    this.setText(chars, 0, chars.size)
}
