package ru.endlesscode.miet.orioks.model

import android.support.annotation.ColorRes

data class DisciplineViewData(
    val id: Int,
    val name: String,
    val controlForm: String,
    val teachers: List<String>?,
    val department: String,
    val currentGrade: String,
    @ColorRes
    val colorId: Int
)
