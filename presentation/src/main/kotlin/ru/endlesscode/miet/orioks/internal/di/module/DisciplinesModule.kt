package ru.endlesscode.miet.orioks.internal.di.module

import dagger.Binds
import dagger.Module
import ru.endlesscode.miet.orioks.internal.di.MockInjection
import ru.endlesscode.miet.orioks.repository.DisciplinesRepository
import ru.endlesscode.orioks.repository.DisciplinesDataRepository
import ru.endlesscode.orioks.repository.DisciplinesMockRepository

@Module
abstract class DisciplinesModule {

    @Binds
    abstract fun provideDataRepository(dataRepository: DisciplinesDataRepository): DisciplinesRepository

    @Binds
    @MockInjection
    abstract fun provideMockRepository(mockRepository: DisciplinesMockRepository): DisciplinesRepository
}
