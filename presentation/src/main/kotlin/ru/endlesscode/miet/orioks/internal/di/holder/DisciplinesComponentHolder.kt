package ru.endlesscode.miet.orioks.internal.di.holder

import ru.endlesscode.miet.orioks.internal.di.component.DisciplinesComponent
import ru.endlesscode.miet.orioks.internal.di.component.MainComponent
import ru.endlesscode.miet.orioks.internal.di.holder.common.SubComponentHolder


class DisciplinesComponentHolder(
    parent: MainComponentHolder
) : SubComponentHolder<DisciplinesComponent, MainComponent>(parent) {

    override fun provideInternal(parentComponent: MainComponent): DisciplinesComponent {
        return parentComponent.disciplinesComponent().build()
    }
}
