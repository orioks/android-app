package ru.endlesscode.miet.orioks.presentation.settings.presenter

import com.arellomobile.mvp.InjectViewState
import ru.endlesscode.miet.orioks.internal.Screens
import ru.endlesscode.miet.orioks.internal.di.SettingsScope
import ru.endlesscode.miet.orioks.presentation.common.presenter.BasePresenter
import ru.endlesscode.miet.orioks.presentation.settings.SettingsView
import ru.endlesscode.miet.orioks.usecase.LogOutUseCase
import ru.terrakok.cicerone.Router
import javax.inject.Inject


@SettingsScope
@InjectViewState
class SettingsPresenter @Inject constructor(
    private val logOutUseCase: LogOutUseCase,
    private val router: Router
) : BasePresenter<SettingsView>() {

    fun onLogOutClicked() {
        viewState.showLogOutConfirmation()
    }

    fun onNavigationClicked() {
        router.exit()
    }

    fun onLogOutConfirmed() {
        safeSubscribe {
            logOutUseCase.logOut()
                .doOnComplete(::goToAuthorization)
                .subscribe()
        }
    }

    private fun goToAuthorization() {
        router.newRootScreen(Screens.AUTH)
    }
}
