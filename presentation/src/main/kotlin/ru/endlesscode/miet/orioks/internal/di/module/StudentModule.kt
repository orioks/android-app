package ru.endlesscode.miet.orioks.internal.di.module

import dagger.Binds
import dagger.Module
import ru.endlesscode.miet.orioks.internal.di.MockInjection
import ru.endlesscode.miet.orioks.repository.StudentRepository
import ru.endlesscode.orioks.repository.StudentDataRepository
import ru.endlesscode.orioks.repository.StudentMockRepository

@Module
abstract class StudentModule {

    @Binds
    @MockInjection
    abstract fun provideMockRepository(mockRepository: StudentMockRepository): StudentRepository

    @Binds
    abstract fun provideDataRepository(dataRepository: StudentDataRepository): StudentRepository
}
