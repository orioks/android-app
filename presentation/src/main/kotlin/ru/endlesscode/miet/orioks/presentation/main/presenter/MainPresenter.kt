package ru.endlesscode.miet.orioks.presentation.main.presenter

import ru.endlesscode.miet.orioks.internal.Screens
import ru.endlesscode.miet.orioks.presentation.common.presenter.BasePresenter
import ru.endlesscode.miet.orioks.presentation.main.view.MainView
import ru.endlesscode.miet.orioks.usecase.AuthUseCase
import ru.terrakok.cicerone.Router
import javax.inject.Inject


class MainPresenter @Inject constructor(
    private val router: Router,
    private val authUseCase: AuthUseCase
) : BasePresenter<MainView>() {

    override fun onFirstViewAttach() {
        goToFirstScreen()
    }

    private fun goToFirstScreen() {
        val screen = when {
            authUseCase.isAuthorized -> Screens.MAIN_MENU
            else -> Screens.AUTH
        }

        router.replaceScreen(screen)
    }
}
