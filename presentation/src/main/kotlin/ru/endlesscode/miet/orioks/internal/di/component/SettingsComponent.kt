package ru.endlesscode.miet.orioks.internal.di.component

import dagger.Subcomponent
import ru.endlesscode.miet.orioks.internal.di.SettingsScope
import ru.endlesscode.miet.orioks.presentation.settings.fragment.SettingsFragment


@SettingsScope
@Subcomponent
interface SettingsComponent {

    fun inject(fragment: SettingsFragment)

    @Subcomponent.Builder
    interface Builder {
        fun build(): SettingsComponent
    }
}
