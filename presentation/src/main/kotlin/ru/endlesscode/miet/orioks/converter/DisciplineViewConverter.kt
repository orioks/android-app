package ru.endlesscode.miet.orioks.converter

import ru.endlesscode.miet.orioks.model.Discipline
import ru.endlesscode.miet.orioks.model.DisciplineViewData

object DisciplineViewConverter {

    fun fromDomain(source: Discipline): DisciplineViewData {
        with(source) {
            return DisciplineViewData(
                id = id,
                name = name,
                controlForm = controlForm ?: "Не назначена", // TODO: Перенести в ресурсы
                department = department,
                teachers = teachers,
                currentGrade = GradeConverter.gradeToString(currentGrade),
                colorId = GradeConverter.gradeToColor(currentGrade, maxGrade)
            )
        }
    }
}
