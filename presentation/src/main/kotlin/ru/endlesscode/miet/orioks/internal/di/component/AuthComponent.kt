package ru.endlesscode.miet.orioks.internal.di.component

import dagger.Subcomponent
import ru.endlesscode.miet.orioks.internal.di.AuthScope
import ru.endlesscode.miet.orioks.presentation.auth.fragment.AuthFragment


@AuthScope
@Subcomponent
interface AuthComponent {

    fun inject(fragment: AuthFragment)

    @Subcomponent.Builder
    interface Builder {
        fun build(): AuthComponent
    }
}
