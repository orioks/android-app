package ru.endlesscode.miet.orioks.presentation.auth.presenter

import com.arellomobile.mvp.InjectViewState
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import retrofit2.HttpException
import ru.endlesscode.miet.orioks.R
import ru.endlesscode.miet.orioks.internal.Screens
import ru.endlesscode.miet.orioks.presentation.auth.view.AuthView
import ru.endlesscode.miet.orioks.presentation.common.presenter.BasePresenter
import ru.endlesscode.miet.orioks.usecase.AuthUseCase
import ru.endlesscode.miet.orioks.util.Validator
import ru.endlesscode.miet.orioks.util.Validator.Companion.BLANK
import ru.endlesscode.miet.orioks.util.Validator.Companion.NO_ERROR
import ru.endlesscode.miet.orioks.util.Validator.Companion.WRONG_LENGTH
import ru.terrakok.cicerone.Router
import timber.log.Timber
import java.net.UnknownHostException
import javax.inject.Inject

@InjectViewState
class AuthPresenter @Inject constructor(
        private val router: Router,
        private val validator: Validator,
        private val authUseCase: AuthUseCase
) : BasePresenter<AuthView>() {

    override fun onFirstViewAttach() {
        restoreCachedFields()
    }

    fun setFieldsValues(loginValues: Observable<CharSequence>, passwordValues: Observable<CharSequence>) {
        safeSubscribe {
            Observable.combineLatest(
                    loginValues.map(this::checkLogin),
                    passwordValues.map(this::checkPassword),
                    BiFunction { loginIsValid: Boolean, passIsValid: Boolean -> loginIsValid && passIsValid }
            ).subscribe(viewState::toggleLoginButton)
        }
    }

    private fun restoreCachedFields() {
        viewState.fillFields(authUseCase.cachedLogin)
    }

    private fun checkLogin(login: CharSequence): Boolean {
        val status = validator.validate(login) {
            notBlank()
            lengthExact(7)
        }

        when (status) {
            BLANK -> viewState.showLoginError(R.string.error_empty_field)
            WRONG_LENGTH -> viewState.showLoginError(R.string.error_wrong_login_length)
            else -> viewState.showLoginError(0)
        }

        return status == NO_ERROR
    }

    private fun checkPassword(password: CharSequence): Boolean {
        val status = validator.validate(password) {
            notBlank()
        }

        when (status) {
            BLANK -> viewState.showPasswordError(R.string.error_empty_field)
            else -> viewState.showPasswordError(0)
        }

        return status == NO_ERROR
    }

    fun onLogInPressed(login: String, password: CharArray) {
        safeSubscribe {
            authUseCase.logIn(login, password)
                .subscribe(::nextScreen, ::onError)
        }
    }

    private fun nextScreen() {
        viewState.closeKeyboard()
        router.newRootScreen(Screens.MAIN_MENU)
    }

    private fun onError(throwable: Throwable) {
        Timber.d(throwable)

        when (throwable) {
            is UnknownHostException -> viewState.showError(R.string.error_no_internet_connection)
            is HttpException -> when (throwable.code()) {
                401 -> viewState.showError(R.string.error_incorrect_credentials)
                else -> router.showSystemMessage(throwable.message())
            }
            else -> router.showSystemMessage(throwable.message)
        }
    }
}
