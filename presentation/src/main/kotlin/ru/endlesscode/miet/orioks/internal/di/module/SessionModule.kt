package ru.endlesscode.miet.orioks.internal.di.module

import dagger.Binds
import dagger.Module
import ru.endlesscode.miet.orioks.internal.di.ApplicationScope
import ru.endlesscode.miet.orioks.provider.ProfileHolder
import ru.endlesscode.orioks.provider.PrefsProfileHolder


@Module
abstract class SessionModule {

    @Binds
    @ApplicationScope
    abstract fun provideProfileHolder(prefsAuthHolder: PrefsProfileHolder): ProfileHolder
}
