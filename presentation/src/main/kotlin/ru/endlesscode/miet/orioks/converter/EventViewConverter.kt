package ru.endlesscode.miet.orioks.converter

import ru.endlesscode.miet.orioks.model.Event
import ru.endlesscode.miet.orioks.model.EventViewData

object EventViewConverter {

    fun fromDomain(source: Event): EventViewData {
        with(source) {
            return EventViewData(
                type = type,
                alias = alias,
                name = name,
                week = week,
                colorId = GradeConverter.gradeToColor(currentGrade, maxGrade),
                currentGrade = GradeConverter.gradeToString(currentGrade),
                maxGrade = GradeConverter.gradeToString(maxGrade)
            )
        }
    }

}
