package ru.endlesscode.miet.orioks.converter

import ru.endlesscode.miet.orioks.R
import java.text.DecimalFormat
import kotlin.math.roundToInt

object GradeConverter {

    private val gradeZones = listOf(0.25, 0.5, 0.7, 0.85, 1.0)
    private val decimalFormat = DecimalFormat("#.#")

    private val gradeColors = arrayOf(
            R.color.grade_red,
            R.color.grade_orange,
            R.color.grade_amber,
            R.color.grade_lime,
            R.color.grade_green
    )

    fun gradeToColor(currentGrade: Float?, maxGrade: Float = 100f): Int {
        if (currentGrade == null) return 0
        val colorId = gradeZones.indexOfFirst { currentGrade <= maxGrade * it }
        return gradeColors[colorId]
    }

    fun gradeToString(grade: Float?): String {
        return when {
            grade == null -> "-"
            grade < 0f -> "н"
            grade > 100f -> grade.roundToInt().toString()
            else -> decimalFormat.format(grade)
        }
    }
}
