package ru.endlesscode.miet.orioks.internal.di.holder.common

import timber.log.Timber


abstract class ComponentHolder<out Component> {

    companion object {
        private const val TAG = "ComponentHolder"
    }

    private var component: Component? = null
        get() {
            linksCount++
            return field
        }
        set(value) {
            linksCount = if (value == null) 0 else 1
            field = value
        }

    private var linksCount: Int = 0

    protected abstract fun provideInternal(): Component

    fun provideComponent(): Component {
        synchronized(this) {
            log("[+] ${this.javaClass.simpleName} : ${this.linksCount} -> ${this.linksCount + 1}")
            return component ?: provideInternal().also { component = it }
        }
    }

    fun onDependencyReleased() {
        synchronized(this) {
            if (linksCount == 1) {
                log("[-] ${this.javaClass.simpleName} : ${this.linksCount} -> destroy")
                destroyComponent()
            } else {
                log("[-] ${this.javaClass.simpleName} : ${this.linksCount} -> ${this.linksCount - 1}")
                linksCount--
            }
        }
    }

    protected open fun onComponentDestroyed() {
    }

    private fun destroyComponent() {
        component = null
        onComponentDestroyed()
    }

    private fun log(message: String) {
        Timber.tag(TAG)
        Timber.d(message)
    }
}
