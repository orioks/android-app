package ru.endlesscode.miet.orioks

import android.app.Application
import ru.endlesscode.miet.orioks.internal.di.DI
import timber.log.Timber

class OrioksApp : Application() {

    override fun onCreate() {
        super.onCreate()

        DI.init(this)

        // There no sensitive debug information. It was removed from Timber logs.
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}
