package ru.endlesscode.miet.orioks.internal.di.holder

import ru.endlesscode.miet.orioks.internal.di.component.MainComponent
import ru.endlesscode.miet.orioks.internal.di.component.SettingsComponent
import ru.endlesscode.miet.orioks.internal.di.holder.common.SubComponentHolder


class SettingsComponentHolder(
    parent: MainComponentHolder
) : SubComponentHolder<SettingsComponent, MainComponent>(parent) {

    override fun provideInternal(parentComponent: MainComponent): SettingsComponent {
        return parentComponent.settingsComponent().build()
    }
}
