package ru.endlesscode.miet.orioks.presentation.settings.fragment

import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.screen_settings.*
import ru.endlesscode.miet.orioks.R
import ru.endlesscode.miet.orioks.internal.di.DI
import ru.endlesscode.miet.orioks.presentation.common.fragment.BaseFragment
import ru.endlesscode.miet.orioks.presentation.settings.SettingsView
import ru.endlesscode.miet.orioks.presentation.settings.presenter.SettingsPresenter
import ru.endlesscode.miet.orioks.util.alert
import ru.endlesscode.miet.orioks.util.negativeButton
import ru.endlesscode.miet.orioks.util.positiveButton
import javax.inject.Inject

class SettingsFragment : BaseFragment(), SettingsView {

    companion object {
        fun newInstance(): SettingsFragment = SettingsFragment()
    }

    override val layoutId = R.layout.screen_settings

    @Inject
    @InjectPresenter
    internal lateinit var presenter: SettingsPresenter

    @ProvidePresenter
    fun providePresenter(): SettingsPresenter = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        DI.settings.provideComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        registerListeners()
    }

    override fun onDestroy() {
        super.onDestroy()

        DI.settings.onDependencyReleased()
    }

    override fun showLogOutConfirmation() {
        context?.let {
            alert(it, R.string.log_out, R.string.log_out_confirmation) {
                positiveButton(android.R.string.yes) { presenter.onLogOutConfirmed() }
                negativeButton(android.R.string.no)
            }
        }
    }

    private fun registerListeners() {
        toolbar.setNavigationOnClickListener { presenter.onNavigationClicked() }
        log_out_button.setOnClickListener { presenter.onLogOutClicked() }
    }
}
