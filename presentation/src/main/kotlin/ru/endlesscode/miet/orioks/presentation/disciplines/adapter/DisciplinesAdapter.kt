package ru.endlesscode.miet.orioks.presentation.disciplines.adapter

import android.content.res.ColorStateList
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_discipline.view.*
import ru.endlesscode.miet.orioks.R
import ru.endlesscode.miet.orioks.model.DisciplineViewData
import ru.endlesscode.miet.orioks.presentation.common.adapter.ItemListAdapter
import ru.endlesscode.miet.orioks.util.backgroundTintListCompat
import ru.endlesscode.miet.orioks.util.getColorCompat
import ru.endlesscode.miet.orioks.util.makeGone


class DisciplinesAdapter : ItemListAdapter<DisciplineViewData>() {

    var onItemClickListener: (DisciplineViewData) -> Unit = {}

    override fun createItemViewHolder(group: ViewGroup): ItemViewHolder = ViewHolder(group)

    override fun initItemViewHolder(holder: ItemViewHolder, item: DisciplineViewData) {
        if (holder !is ViewHolder) return
        holder.init(item)
    }

    private inner class ViewHolder(
            parent: ViewGroup
    ) : ItemListAdapter.ItemViewHolder(parent, R.layout.item_discipline) {

        private lateinit var discipline: DisciplineViewData

        fun init(item: DisciplineViewData) {
            this.discipline = item

            itemView.init()
            registerListeners()
        }

        private fun View.init() {
            with(discipline) {
                // TODO: on this week
                week_indicator_image_view.makeGone()
                discipline_title_text_view.text = name
                type_text_view.text = controlForm
                rank_text_view.text = currentGrade

                if (colorId != 0) {
                    val color = context.getColorCompat(colorId)
                    rank_text_view.backgroundTintListCompat = ColorStateList.valueOf(color)
                }
            }
        }

        private fun registerListeners() {
            itemView.setOnClickListener { onItemClickListener.invoke(discipline) }
        }

    }
}
