package ru.endlesscode.miet.orioks.internal.di.holder

import ru.endlesscode.miet.orioks.internal.di.component.ApplicationComponent
import ru.endlesscode.miet.orioks.internal.di.component.MainComponent
import ru.endlesscode.miet.orioks.internal.di.holder.common.SubComponentHolder


class MainComponentHolder(
    parent: ApplicationComponentHolder
) : SubComponentHolder<MainComponent, ApplicationComponent>(parent) {

    val disciplines: DisciplinesComponentHolder by lazy { DisciplinesComponentHolder(this) }
    val settings: SettingsComponentHolder by lazy { SettingsComponentHolder(this) }

    override fun provideInternal(parentComponent: ApplicationComponent): MainComponent {
        return parentComponent.mainComponent().build()
    }
}
