package ru.endlesscode.miet.orioks.converter

import ru.endlesscode.miet.orioks.model.Student
import ru.endlesscode.miet.orioks.model.StudentViewData

object StudentViewConverter {

    fun fromDomain(source: Student): StudentViewData {
        with(source) {
            return StudentViewData(
                fullName = fullName,
                group = group,
                recordBook = recordBook.toString()
            )
        }
    }
}
