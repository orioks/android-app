package ru.endlesscode.miet.orioks.internal.di.module

import dagger.Binds
import dagger.Module
import ru.endlesscode.miet.orioks.provider.BasicAuthProvider
import ru.endlesscode.orioks.provider.OrioksAuthProvider


@Module
interface AuthModule {

    @Binds
    fun provideGitHubAuthProvider(orioksAuthProvider: OrioksAuthProvider): BasicAuthProvider
}
