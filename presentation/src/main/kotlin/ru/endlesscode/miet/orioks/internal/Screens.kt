package ru.endlesscode.miet.orioks.internal

object Screens {
    const val MAIN_MENU = "main_menu"
    const val AUTH = "auth"
    const val DISCIPLINE = "discipline"

    const val PROGRESS = "progress"
    const val FAQ = "faq"
    const val NEWS = "news"
    const val SCHEDULE = "schedule"
    const val SETTINGS = "settings"
}
