package ru.endlesscode.miet.orioks.presentation.auth.view

import android.support.annotation.StringRes
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.OneExecutionStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType


@StateStrategyType(SkipStrategy::class)
interface AuthView : MvpView {

    fun showError(
            @StringRes
            messageId: Int
    )

    fun showLoginError(
            @StringRes
            messageId: Int
    )

    fun showPasswordError(
            @StringRes
            messageId: Int
    )

    @StateStrategyType(SingleStateStrategy::class)
    fun toggleLoginButton(enabled: Boolean)

    @StateStrategyType(SingleStateStrategy::class)
    fun fillFields(newLogin: String)

    @StateStrategyType(OneExecutionStateStrategy::class)
    fun closeKeyboard()
}
