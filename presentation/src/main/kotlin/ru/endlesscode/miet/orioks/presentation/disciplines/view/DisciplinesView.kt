package ru.endlesscode.miet.orioks.presentation.disciplines.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SingleStateStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.endlesscode.miet.orioks.model.DisciplineViewData


@StateStrategyType(SingleStateStrategy::class)
interface DisciplinesView : MvpView {

    fun initDisciplines(disciplines: List<DisciplineViewData>)
}
