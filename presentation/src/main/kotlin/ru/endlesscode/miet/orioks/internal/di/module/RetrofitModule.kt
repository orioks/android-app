package ru.endlesscode.miet.orioks.internal.di.module

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.endlesscode.miet.orioks.internal.di.ApplicationScope
import ru.endlesscode.orioks.network.interceptor.BasicAuthenticator
import ru.endlesscode.orioks.network.interceptor.ResponseInterceptor


@Module
object RetrofitModule {

    @JvmStatic
    @Provides
    @ApplicationScope
    fun provideRetrofit(
        builder: Retrofit.Builder,
        gsonConverterFactory: GsonConverterFactory,
        rxJava2CallAdapterFactory: RxJava2CallAdapterFactory
    ): Retrofit {
        return builder.apply {
            baseUrl("https://orioks.miet.ru/api/v1/")
            addConverterFactory(gsonConverterFactory)
            addCallAdapterFactory(rxJava2CallAdapterFactory)
        }.build()
    }

    @JvmStatic
    @Provides
    fun provideRetrofitBuilder(client: OkHttpClient): Retrofit.Builder = Retrofit.Builder().client(client)

    @JvmStatic
    @Provides
    fun provideHttpClient(
        certificatePinner: CertificatePinner,
        authenticator: BasicAuthenticator,
        responseInterceptor: ResponseInterceptor
    ): OkHttpClient {
        val interceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        return OkHttpClient.Builder()
            .certificatePinner(certificatePinner)
            .addInterceptor(authenticator)
            .addInterceptor(responseInterceptor)
            .addInterceptor(interceptor)
            .build()
    }

    @JvmStatic
    @Provides
    fun provideCertificatePinner(): CertificatePinner {
        return CertificatePinner.Builder()
            .add("*.miet.ru", "sha256/Ki/Pv9lPp+wqn0jl/TyvJp49NkMEd6U4h8k6LwxW26Y=")
            .build()
    }

    @JvmStatic
    @Provides
    fun provideGsonConverterFactory(gson: Gson): GsonConverterFactory = GsonConverterFactory.create(gson)

    @JvmStatic
    @Provides
    @ApplicationScope
    fun provideGson(): Gson {
        return GsonBuilder().apply {
            setPrettyPrinting()
            setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
        }.create()
    }

    @JvmStatic
    @Provides
    fun provideRxJava2CallAdapterFactory(): RxJava2CallAdapterFactory = RxJava2CallAdapterFactory.create()
}
