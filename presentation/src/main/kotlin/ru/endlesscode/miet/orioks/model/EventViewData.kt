package ru.endlesscode.miet.orioks.model

import android.support.annotation.ColorRes

data class EventViewData(
    val type: String,
    val alias: String?,
    val name: String?,
    val week: Int,
    val currentGrade: String,
    val maxGrade: String,
    @ColorRes
    val colorId: Int
)
