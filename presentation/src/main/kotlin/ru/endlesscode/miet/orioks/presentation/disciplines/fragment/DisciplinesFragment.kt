package ru.endlesscode.miet.orioks.presentation.disciplines.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.arellomobile.mvp.presenter.ProvidePresenter
import kotlinx.android.synthetic.main.screen_discipline_list.*
import ru.endlesscode.miet.orioks.R
import ru.endlesscode.miet.orioks.internal.di.DI
import ru.endlesscode.miet.orioks.model.DisciplineViewData
import ru.endlesscode.miet.orioks.presentation.common.fragment.BaseFragment
import ru.endlesscode.miet.orioks.presentation.disciplines.adapter.DisciplinesAdapter
import ru.endlesscode.miet.orioks.presentation.disciplines.presenter.DisciplinesPresenter
import ru.endlesscode.miet.orioks.presentation.disciplines.view.DisciplinesView
import ru.endlesscode.miet.orioks.util.makeGone
import javax.inject.Inject


class DisciplinesFragment : BaseFragment(), DisciplinesView {

    companion object {
        fun newInstance(): DisciplinesFragment = DisciplinesFragment()
    }

    override val layoutId = R.layout.screen_discipline_list

    private val disciplinesAdapter by lazy { DisciplinesAdapter() }
    private val debtAdapter by lazy { DisciplinesAdapter() }

    @Inject
    @InjectPresenter
    internal lateinit var presenter: DisciplinesPresenter

    @ProvidePresenter
    fun providePresenter(): DisciplinesPresenter = presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        DI.disciplines.provideComponent().inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        disciplines_list.init(disciplinesAdapter)
        initDebts()
    }

    override fun initDisciplines(disciplines: List<DisciplineViewData>) {
        disciplinesAdapter.initItems(disciplines)
        disciplinesAdapter.onItemClickListener = presenter::onDisciplineClicked
    }

    override fun onDestroy() {
        super.onDestroy()

        DI.disciplines.onDependencyReleased()
    }

    private fun initDebts() {
        val debts = emptyList<DisciplineViewData>()
        if (debts.isEmpty()) {
            debts_title_text_view.makeGone()
            debts_list.makeGone()
            return
        }

        debts_list.init(debtAdapter)
        debtAdapter.initItems(debts)
        debtAdapter.onItemClickListener = presenter::onDisciplineClicked
    }

    private fun RecyclerView.init(adapter: DisciplinesAdapter) {
        setHasFixedSize(true)
        layoutManager = LinearLayoutManager(context)
        if (this.adapter == null) this.adapter = adapter
    }
}
