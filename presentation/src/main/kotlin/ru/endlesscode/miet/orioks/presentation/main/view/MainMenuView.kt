package ru.endlesscode.miet.orioks.presentation.main.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import ru.endlesscode.miet.orioks.model.StudentViewData


@StateStrategyType(AddToEndSingleStrategy::class)
interface MainMenuView : MvpView {

    fun showStudentData(data: StudentViewData)
}
