package ru.endlesscode.miet.orioks.internal.di.component

import dagger.Subcomponent
import ru.endlesscode.miet.orioks.internal.di.DisciplinesScope
import ru.endlesscode.miet.orioks.internal.di.module.DisciplinesModule
import ru.endlesscode.miet.orioks.presentation.disciplines.fragment.DisciplineFragment
import ru.endlesscode.miet.orioks.presentation.disciplines.fragment.DisciplinesFragment


@DisciplinesScope
@Subcomponent(modules = [DisciplinesModule::class])
interface DisciplinesComponent {

    fun inject(fragment: DisciplinesFragment)
    fun inject(fragment: DisciplineFragment)

    @Subcomponent.Builder
    interface Builder {
        fun build(): DisciplinesComponent
    }
}
